using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class ProjectInstaller : MonoInstaller<ProjectInstaller>
{
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);
        Container.BindInterfacesAndSelfTo<SoundZenMan>()
            .FromResource("AudioMenager")
            .AsSingle()
            .OnInstantiated(
            (InjectContext context, SoundZenMan zen) => { Debug.Log("test"); zen.ManualInject(Container); })
            .NonLazy();
    }

}
