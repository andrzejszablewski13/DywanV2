using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using FMOD.Studio;
using Zenject;

[CreateAssetMenu(fileName ="AudioMenager",menuName ="AudioMen")]
    public class SoundZenMan : ScriptableObject, IAudioPlayer, IToggleAudio, IAudioVolume
{
        [SerializeField] [Range(0f, 1f)] private float _globalVolume = 1f;
    [SerializeField] [Range(0f, 1f)] private float _musicVolume = 1f;
    [SerializeField] [Range(0f, 1f)] private float _soundVolume = 1f;
    [SerializeField] EventReference _ambient;
    [System.Serializable]
    //class that contains all sound ref
    public class Sound
    {
        [Tooltip("key to activate sound ")]
        public string key;//key to activate sound
        [Tooltip("max volume of sound")]
        [Range(0f, 1f)] public float soundVolume = 1;//max volume of sound
        [Tooltip("clips - from them one is randomly selected to be played")]
        public EventReference[] clips;//clips - from them one is randomly selected to be played
        [HideInInspector]
        public int simultaneousPlayCount = 0;//track of number actual playing sounds of this object
    }


    [Header("Max number allowed of same sounds playing together")]
        public int maxSimultaneousSounds = 7;
        [Tooltip("Update this list in runtime dont work. Script on start create a dictionary from this list. Keays are values names key in Sound object ")]
        public List<Sound> _sounds = new List<Sound>();
        private Dictionary<string, Sound> _soundDictionary;

    private StudioEventEmitter _musicSource;
        private const string MUTE_PREF_KEY = "MutePreference";
        private const int MUTED = 1;
        private const int UN_MUTED = 0;
        private const string MUSIC_PREF_KEY = "MusicPreference";
        private const int MUSIC_OFF = 0;
        private const int MUSIC_ON = 1;
    //refs for IAudioVolume
    public float MusicVolume { get => _musicVolume; set { _musicVolume = Mathf.Clamp01(value); _musicSource.EventInstance.setVolume( _soundDictionary["background"].soundVolume * _globalVolume * _musicVolume); } }
    public float SoundVolume { get => _soundVolume; set { _soundVolume = Mathf.Clamp01(value);  } }
    public float GlobalVolume { get => _globalVolume; set { _globalVolume = Mathf.Clamp01(value); _musicSource.EventInstance.setVolume(_soundDictionary["background"].soundVolume * _globalVolume * _musicVolume); } }
    private AudioPrefab.Pool _audioPool;
    public void ManualInject(DiContainer container)
    {
        container.Inject(this);
    }
    [Inject]
    private void InjectData(AudioPrefab.Pool audioPool)
    {
        _audioPool = audioPool;
        _soundDictionary = new Dictionary<string, Sound>();
        for (int i = 0; i < _sounds.Count; i++)
        {
            _soundDictionary.Add(_sounds[i].key, _sounds[i]);
        }
        if (_musicSource == null)
        {
            _musicSource = _audioPool.Spawn(Vector3.zero).GetComponent<StudioEventEmitter>();
            DontDestroyOnLoad(_musicSource.gameObject);
        }
        PlayMusic(_ambient);
    }


    /// <summary>
    /// play audo at 0,0,0
    /// </summary>
    /// <param name="key">key to sound ref</param>
    /// <param name="autoScaleVolume">scale volume depend on its actual playing number in scene</param>
    /// <param name="maxVolumeScale">max volume value</param>
    public StudioEventEmitter PlaySound(string key, bool autoScaleVolume = true, bool autoKill = true, float maxVolumeScale = 1f)
        {
           return CRPlaySound(_soundDictionary[key],Vector3.zero, autoScaleVolume, autoKill, maxVolumeScale);

        }
    /// <summary>
    /// play audio at given position
    /// </summary>
    /// <param name="key">key to sound ref</param>
    /// <param name="pos">at what pos to spawn</param>
    /// <param name="autoScaleVolume">scale volume depend on its actual playing number in scene</param>
    /// <param name="maxVolumeScale">max volume value</param>
    public StudioEventEmitter PlaySound(string key,Vector3 pos, bool autoScaleVolume = true, bool autoKill = true, float maxVolumeScale = 1f)
    {
        return CRPlaySound(_soundDictionary[key], pos, autoScaleVolume,autoKill, maxVolumeScale); 

    }
    public StudioEventEmitter PlaySound(string key, Transform parent, bool autoScaleVolume = true, bool autoKill = true, float maxVolumeScale = 1f)
    {
       var t= CRPlaySound(_soundDictionary[key], Vector3.zero, autoScaleVolume, autoKill, maxVolumeScale);
        t.transform.SetParent(parent);
        t.transform.position = Vector3.zero;
        return t;
    }
    /// <summary>
    /// sound player and component recycler
    /// </summary>
    /// <param name="sound">sound ref</param>
    /// <param name="pos">at what pos to spawn</param>
    /// <param name="autoScaleVolume">scale volume depend on its actual playing number in scene</param>
    /// <param name="maxVolumeScale">max volume value</param>
    /// <returns>work in time</returns>


    private StudioEventEmitter CRPlaySound(Sound sound,Vector3 pos, bool autoScaleVolume = true,bool autoKill=true, float maxVolumeScale = 1f)
    {
        if (sound.simultaneousPlayCount >= maxSimultaneousSounds)
        {
            return null;
        }

        sound.simultaneousPlayCount++;

        float vol = maxVolumeScale * sound.soundVolume * _globalVolume;

        // Scale down volume of same sound played subsequently
        if (autoScaleVolume && sound.simultaneousPlayCount > 0)
        {
            vol /= (float)(sound.simultaneousPlayCount);
        }
        AudioPrefab prefab = _audioPool.Spawn(pos);
        StudioEventEmitter audioSorce = prefab.GetComponent<StudioEventEmitter>();
        //audioSorce.mute = IsMuted();

        audioSorce.EventReference = (sound.clips[Random.Range(0, sound.clips.Length)]);
        audioSorce.Lookup();

        audioSorce.Play();
        //audioSorce.EventDescription.get(out float time);
        if (autoKill)
        {
            prefab.StartCoroutine(DespawnOnEnd(audioSorce, sound));
        }
        audioSorce.EventInstance.setVolume(IsMuted() ? 0 : vol);
        return audioSorce;
    }
    IEnumerator DespawnOnEnd(StudioEventEmitter audioSorce, Sound sound)
    {
        yield return new WaitWhile(audioSorce.IsPlaying);
        Unspawn(audioSorce, sound);
    }
    public void Unspawn(StudioEventEmitter audioSorce, Sound sound)
    {
        sound.simultaneousPlayCount--;
        audioSorce.Preload = false;
        audioSorce.EventDescription.unloadSampleData();
        _audioPool.Despawn(audioSorce.GetComponent<AudioPrefab>());
    }
    public StudioEventEmitter PlayMusic(EventReference evRef,Vector3 pos =default(Vector3))
    {
        _musicSource.transform.position = pos;
        _musicSource.EventReference = evRef;
        _musicSource.Lookup();
        _musicSource.Play();


        _musicSource.EventInstance.setVolume(!(!IsMuted() && !IsMusicOff()) ? 0 : _globalVolume);
        return _musicSource;
    }

    //for IToggleAudio

    public bool IsMuted()
        {
            return (PlayerPrefs.GetInt(MUTE_PREF_KEY, UN_MUTED) == MUTED);
        }

        public bool IsMusicOff()
        {
            return (PlayerPrefs.GetInt(MUSIC_PREF_KEY, MUSIC_ON) == MUSIC_OFF);
        }

        /// <summary>
        /// Toggles the mute status.
        /// </summary>
        public void ToggleMute()
        {
            // Toggle current mute status
            
            if (!IsMuted())
            {
                // Muted
                PlayerPrefs.SetInt(MUTE_PREF_KEY, MUTED);
                _musicSource.EventInstance.setVolume(0);
        }
            else
            {
                // Un-muted
                PlayerPrefs.SetInt(MUTE_PREF_KEY, UN_MUTED);
                _musicSource.EventInstance.setVolume(IsMusicOff() ? 0 : _globalVolume);

        }
        }


        /// <summary>
        /// Toggles the mute status.
        /// </summary>
        public void ToggleMusic()
        {

            if (IsMusicOff())
            {
                PlayerPrefs.SetInt(MUSIC_PREF_KEY, MUSIC_ON);
                _musicSource.EventInstance.setVolume(IsMuted() ? 0 : _globalVolume);
        }
            else
            {
                PlayerPrefs.SetInt(MUSIC_PREF_KEY, MUSIC_OFF);
                _musicSource.EventInstance.setVolume(0);
        }
        }


    }
