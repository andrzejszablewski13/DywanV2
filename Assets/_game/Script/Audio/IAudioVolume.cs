﻿/// <summary>
/// interface to control audio volume
/// </summary>
public interface IAudioVolume
{
    public float MusicVolume { get; set; }
    public float SoundVolume { get; set; }
    public float GlobalVolume { get; set; }
}
