﻿using FMODUnity;
using UnityEngine;
/// <summary>
/// interface to play audio 
/// </summary>
public interface IAudioPlayer
{
    public StudioEventEmitter PlaySound(string key, bool autoScaleVolume = true,bool autoKill=true, float maxVolumeScale = 1f);
    public StudioEventEmitter PlaySound(string key, Vector3 pos, bool autoScaleVolume = true, bool autoKill = true, float maxVolumeScale = 1f);

    public StudioEventEmitter PlaySound(string key, Transform parent, bool autoScaleVolume = true, bool autoKill = true, float maxVolumeScale = 1f);
    public StudioEventEmitter PlayMusic(EventReference evRef, Vector3 pos = default(Vector3));
}

