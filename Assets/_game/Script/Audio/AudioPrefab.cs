using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class AudioPrefab : MonoBehaviour
{
    public class Pool : MonoMemoryPool<Vector3,AudioPrefab>
    {
        protected override void Reinitialize(Vector3 p1, AudioPrefab item)
        {
            base.Reinitialize(p1, item);
            item.transform.position = p1;
        }
    }
}
