using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using com.zibra.liquid.Manipulators;
public class RaycastClean : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Texture2D dirtMaskTexture;
    [SerializeField] private GameObject _carpet,_torus,_fluidEmitter;
    private ZibraLiquidDetector[] _detectors;
    private Texture2D _cleaningTexture;
    private void Awake()
    {
        _detectors = _torus.GetComponentsInChildren<ZibraLiquidDetector>();

    }
    void Start()
    {
        _cleaningTexture = new Texture2D(dirtMaskTexture.width, dirtMaskTexture.height);
        _cleaningTexture.SetPixels(dirtMaskTexture.GetPixels());
        _cleaningTexture.Apply();
        _carpet.GetComponent<Renderer>().sharedMaterial.SetTexture("_DirtMask", _cleaningTexture);
    }
    private void ChangeMask()
    {
        Ray ray = new Ray(_fluidEmitter.transform.position, _fluidEmitter.transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * 25, Color.red, 1);
        if (Physics.Raycast(ray, out RaycastHit raycastHit) && raycastHit.collider.gameObject.name== "carpet")
        {
            Vector3 textureCoord = raycastHit.textureCoord;

            int pixelX = (int)(textureCoord.x * _cleaningTexture.width);
            int pixelY = (int)(textureCoord.y * _cleaningTexture.height);
            int pixelXOffset = pixelX - 125;
            int pixelYOffset = pixelY - 125;
                    for (int x = 0; x < 125; x++)
                    {
                        for (int y = 0; y < 125; y++)
                        {
                            _cleaningTexture.SetPixel(
                            pixelXOffset + x,
                            pixelYOffset + y,
                            Color.white
                            );
                        }

                    }

            
        }
    }
    private void FixedUpdate()
    {
        Ray ray = new Ray(_fluidEmitter.transform.position, _fluidEmitter.transform.forward);
        if (Physics.Raycast(ray, out RaycastHit raycastHit))
        {
            _torus.transform.position = raycastHit.point;
            _torus.transform.rotation = Quaternion.FromToRotation(Vector3.up, raycastHit.normal);
        }

        ChangeMask();

        _cleaningTexture.Apply();
    }
}
